
#include <sys/wait.h>
#include <unistd.h>
#include <stdlib.h>
#include <stdio.h>
#include <string.h>

//define functions
int cd(char **args);
int help(char **args);
int quit(char **args);
int echo(char **args);
//builtin commands names to be executed without exec function
char *builtin_str[] = {
  "cd",
  "help",
  "quit", 
  "echo"
};
//builtin commands function locations  
int (*builtin_func[]) (char **) = {
  &cd,
  &help,
  &quit, 
  &echo
};
//return number of strings in this array
int lsh_num_builtins() {
  return sizeof(builtin_str) / sizeof(char *);
}
//function for "cd" command in shell
int cd(char **args){
  //if no argument was passed, print stderr
  if (args[1] == NULL) {
    fprintf(stderr, "Error: expected argument to \"cd\"\n");
  } else {
    if (chdir(args[1]) != 0) {
      //if the given argument does not exist in given directory, print error
      perror("Error");
    }
  }
  //success and continue to be in shell environment
  return 1;
}
//function for "help" command in shell
int help(char **args) {
  int i;
  printf("**** NEGRA ****** Help ****\n\nBuilt-in functions : "); 
  //print functions names and recommendation for "man" pages.
  for (i = 0; i < lsh_num_builtins(); i++) {
    printf("\t%s\n", builtin_str[i]);
  }
  printf("\nUse the \"man\" command for information on other programs.\n");
  return 1;
}
//function for "quit"
int quit(char **args){
  return 0;
}
//function for executing other commands using a process for them and passing command and agruments to "execvp"
int exec(char **args){
  pid_t pid;
  int status;
  pid = fork();
  if (pid == 0) {
    // Child process
    if (execvp(args[0], args) == -1) {
      perror("Error");
    }
    exit(EXIT_FAILURE);
  } else if (pid < 0) {
    // Error forking
    perror("Error");
  } else {
    // Parent process
    do {
      waitpid(pid, &status, WUNTRACED);
    } while (!WIFEXITED(status) && !WIFSIGNALED(status));
  }
  return 1;
}
//function for executing commands. For built-in commands uses their fucntion and for others uses exec.
int run_command(char **args){
  int i;
  if (args[0] == NULL) {
    return 1;
  }
  for (i = 0; i < lsh_num_builtins(); i++) {
    if (strcmp(args[0], builtin_str[i]) == 0) {
      return (*builtin_func[i])(args);//built-in commands
    }
  }
  return exec(args);//other commands
}
//function for getting each line character by character.
char *get_line(void){
  int bufsize = 1024;
  int i = 0;
  char *buffer = malloc(sizeof(char) * bufsize);
  int c;
  if (!buffer) {
    fprintf(stderr, "Error: allocation fault\n");
    exit(EXIT_FAILURE);
  }
  while (1) {
    c = getchar();
    if (c == EOF) {
      exit(EXIT_SUCCESS);
    } else if (c == '\n') {
      buffer[i] = '\0';
      return buffer;
    } else {
      buffer[i] = c;
    }
    i++;
    if (i >= bufsize) {
      bufsize += 1024;
      buffer = realloc(buffer, bufsize); //if buffer size was not enough resize it
      if (!buffer) {
        fprintf(stderr, "Error: allocation fault\n");
        exit(EXIT_FAILURE);
      }
    }
  }
}

char* substring(char *c, int i, int j)
{ 
  char *start = &c[i];
  char *end = &c[j]; 
  char *substr = (char *)calloc(1, end - start + 1);
  memcpy(substr, start, end - start);
  return substr;
}

char* concat(const char *s1, const char *s2)
{
    char *result = malloc(strlen(s1)+strlen(s2)+1); 
    strcpy(result, s1);
    strcat(result, s2);
    return result;
}

//write text inside file, depending on append value, if 1 append str to file else write from beginning
int text_to_file(char *str, char *path, int append){
   FILE * fp;
   int i;
   char *c;
   if(append) c = "a"; else c = "w"; 
   fp = fopen (path,c); 
   fprintf (fp, "%s\n",str); 
   fclose (fp);
   return 1;
 }
//function for "echo" command
 int echo(char **args){
   int print = 0; 
   
   //replace environment variables
 	int k = 0;
	while(args[k]!=NULL){
		if(args[k][0] == '$'){
			args[k] = getenv(substring(args[k], 1, 
			(strlen(args[k]) + 1) * sizeof(char)));
		}
		k++;
	}

   if (args[1] == NULL) { //echo without any arg
    printf("\n"); 
    } else if(args[2] == NULL){ // echo with one arg
     printf("%s\n", args[1]); 
   
 } else { //echo with more then one arg 
    print = 1;//flag for printing all of rest args
    int index = 2;
    char *prev = args[1];//string to print and is changes during process
    while(args[index] != NULL){
      if(strlen(args[index]) == 2 &&
        args[index][0] == '>' && args[index][1] == '>'){
        if(args[index + 1] != NULL){ //append prev to file
          print = 0;
          text_to_file(prev, args[index + 1], 1);
        } else {
		fprintf(stderr, "Error: no file argument passed\n");
	}
      } else if(strlen(args[index]) == 1 && args[index][0] == '>'){
        if(args[index + 1] != NULL){ //write prev in file 
          print = 0;
          text_to_file(prev, args[index + 1], 0);
        } else {
		fprintf(stderr, "Error: no file argument passed\n");
	}
      } else { 
        prev = concat(prev, " ");
        prev = concat(prev, args[index]);
      }
      index++;
    }

    if(print) //print all args
    {
      int i = 1;
      while(args[i] != NULL){
        printf("%s ", args[i++]);
      }
      printf("\n"); 
    }
 
}
return 1; // res is 1 always
}
//function for parsing the line and getting arg seperated by space outside of "". Inside each "" is one absolute argument.
char **parse(char *line) 
{
  int bufsize = 64, position = 0;
  char **tokens = malloc(bufsize * sizeof(char*));
  char *token;
  if (!tokens) {
    fprintf(stderr, "Error: allocation fault\n");
    exit(EXIT_FAILURE);
  }

  char **tb;
  int i = 0;
  int j = 0;
  int k = 0; 
  int a = strlen(line)+1;
  for(i = 0; i < a; i++){
   if(line[i] == '"'){ 
    for(j = i + 1; j < a; j++){
     if(line[j] == '"'){ 

      if(k >= bufsize){
        bufsize += 64;
        tb = tokens;
        tokens = realloc(tokens, bufsize * sizeof(char*));
        if(!tokens){
          free(tb);fprintf(stderr, "Error: allocation fault\n");exit(EXIT_FAILURE);
        }
      }

      tokens[k++] = substring(line, i + 1, j);
      i = j;
      break;
    }
  }
} else if(line[i] == ' ') {

}else {
  for(j = i; j < a; j++){
   if(line[j] == ' ' || line[j] == '\0'){ 
    if(k >= bufsize){
        bufsize += 64;
        tb = tokens;
        tokens = realloc(tokens, bufsize * sizeof(char*));
        if(!tokens){
          free(tb);fprintf(stderr, "Error: allocation fault\n");exit(EXIT_FAILURE);
        }
      }
    tokens[k++] = substring(line, i, j);
    i = j;
    break;
  }
}
}   
}

tokens[k] = NULL;


int pointer = 0; 
char **nts = malloc(bufsize * sizeof(char*));
char *s;
while(tokens[pointer] != NULL){
  char *cur = malloc(strlen(tokens[pointer]));
  k = 0; j = 0; i = 0; 
  while(tokens[pointer][k] != NULL){
  char *temp = malloc(strlen(tokens[pointer]));
  j = 0;
    if(tokens[pointer][k] == '$'){
      k++; 
      while(tokens[pointer][k] != ' ' && tokens[pointer][k] != '\0'){
          temp[j++] = tokens[pointer][k++];
      }
      cur = concat(cur, getenv(temp));
      i = strlen(cur);
    } else 
      {
          cur[i] = tokens[pointer][k];
          k++; i++;
      }
  }


  nts[pointer] = cur;  
  pointer++;
}
nts[pointer] = NULL; 
return nts; 
}

//loop for getting commands and processing them.
void loop(void){
  char *line;
  char **args;
  int loop;
  char *usr = getenv("USER");
  do {
    printf("%s$ ", usr);
    line = get_line();
    args = parse(line);
    loop = run_command(args);
  } while (loop);
}


int main(int argc, char **argv){ 
  loop(); 
  return EXIT_SUCCESS;
}

