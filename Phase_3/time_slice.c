#include <sched.h>
#include<stdio.h>
int main(){

	struct timespec tp;

	long i;

	for(i = 0; i < 1000000000; i++){
		long k = 10123;
		k = k + i / 345345;
	}

	//This function writes value of time slice or quantum into given struct.
	//If pid == 0 writes for calling process. 	
	int time_slice = sched_rr_get_interval(0, &tp);

	printf("status :  %d\n", time_slice);//on success 0
	printf("time slice seconds : %ld\n", tp.tv_sec);//seconds of quantum	
	printf("time slice nanoseconds : %ld\n", tp.tv_nsec);//nanoseconds of quantom
	return 0;
}

