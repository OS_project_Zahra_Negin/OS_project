





/*
 
	Output of gettimeofday(&tv, NULL) is stored in the given struct. 
	It shows seconds and milliseconds from 1 January 1970, 00:00:00.
	Using functions and available methods, we convert time from second 
	to date format and output will be current time, year, month, day, hour,
	minute and second. 
	Here we have used www.cplusplus.com/reference/ctime/localtime/ 

*/


#include <stdio.h>
#include <sys/time.h>
#include <time.h>
int main() {
	//This struct contains time in second and millisecond format.
	struct timeval tv;
	//This struct is going to hold corresponding time for local zone.
	struct tm* time_string;
	//This buffer is going to hold time in the given format in strftime function.
	char buffer[26];
	//Here system call occurs and current time from given date in above comments is stored in &tv.
	gettimeofday(&tv, NULL);
	//In this function call, seconds from time struct tv are converted to time zone format. 
	time_string = localtime(&tv.tv_sec);
	//Using strftime time in time_string struct is available in a string to print.
	strftime(buffer, 26, "%Y.%m.%d %H:%M:%S", time_string);
	printf("%s\n", buffer);
	return 0;
}
