#include<stdio.h>
#include <stdlib.h>
#include<unistd.h>

/*

In this code we are trying to get cpu usage and read cpu usage from "/proc/stat" and shows with specific time delays.
This delay depend could be dependant on input value.
if set an num input for this code you can see usage of cpu in last that num ...  

*/

int main(int argc, char *argv[])
{
    long double a[7],user, nice, system, idle, iowait, irq, softirq;
    FILE *fp;
    char dump[50];


       	 fp = fopen("/proc/stat","r");
     	 fscanf(fp,"%*s %Lf %Lf %Lf %Lf %Lf %Lf %Lf",&a[0],&a[1],&a[2],&a[3],&a[4],&a[5],&a[6] );
         fclose(fp);
  if(argc == 1){
         for(;;){

         user = 100 * ((a[0]) / (a[0]+a[1]+a[2]+a[3]+a[4]+a[5]+a[6])); // user: normal processes executing in user mode
	 nice = 100 * ((a[1]) / (a[0]+a[1]+a[2]+a[3]+a[4]+a[5]+a[6]));// nice: niced processes executing in user mode
	 system = 100 * ((a[2]) / (a[0]+a[1]+a[2]+a[3]+a[4]+a[5]+a[6]));// system: processes executing in kernel mode
	 idle = 100* ( (a[3]) / (a[0]+a[1]+a[2]+a[3]+a[4]+a[5]+a[6]));//idle: twiddling thumbs
	 iowait = 100 * ((a[4]) / (a[0]+a[1]+a[2]+a[3]+a[4]+a[5]+a[6]));// iowait: waiting for I/O to complete
	 irq = 100 * ((a[5]) / (a[0]+a[1]+a[2]+a[3]+a[4]+a[5]+a[6]));//irq: servicing interrupts
	 softirq = 100 * ((a[6]) / (a[0]+a[1]+a[2]+a[3]+a[4]+a[5]+a[6]));//softirq: servicing softirqs
	
         printf("The user CPU utilization up till  now is : %Lf%%\n",user);
 	 printf("The nice CPU utilization up till now is : %Lf%%\n ",nice);
	 printf("The system CPU utilization up till now is : %Lf%%\n",system);
	 printf("The idle CPU utilization up till now is : %Lf%%\n ",idle);
	 printf("The iowait CPU utilization up till now is : %Lf%%\n",iowait);
	 printf("The irq CPU utilization up till now is : %Lf%%\n",irq);
	 printf("The softirq CPU utilization up till now is : %Lf%%\n \n\n\n",softirq);
	
	sleep(2); // every two second printh cpu utilization form last boot to now
	}
    }

    if(argc == 2){

	long double b[7],user_d, nice_d, system_d, idle_d, iowait_d, irq_d, softirq_d;
	
	char *p;
	//int time = 10;
	int time = strtol(argv[1], &p, 10); 
	 for(;;){
	sleep(time);
         fp = fopen("/proc/stat","r");
         fscanf(fp,"%*s %Lf %Lf %Lf %Lf %Lf %Lf %Lf",&b[0],&b[1],&b[2],&b[3],&b[4],&b[5],&b[6] );
         fclose(fp);
 	int sum_a = a[0]+a[1]+a[2]+a[3]+a[4]+a[5]+a[6];
	int sum_b = (b[0]+b[1]+b[2]+b[3]+b[4]+b[5]+b[6]);
         user_d = 100 * ((b[0] - a[0]) / ( sum_b  - sum_a)); // user: normal processes executing in user mode
         nice_d = 100 * ((b[1] - a[1]) / ( sum_b  - sum_a ));// nice: niced processes executing in user mode
         system_d = 100 * ((b[2] - a[2]) / (  sum_b  - sum_a ));// system: processes executing in kernel mode
         idle_d = 100* ((b[3] - a[3]) / ( sum_b  - sum_a ));//idle: twiddling thumbs
         iowait_d = 100 * ((b[4] - a[4]) / ( sum_b  - sum_a));// iowait: waiting for I/O to complete
         irq_d = 100 * ((b[5] - a[5]) / ( sum_b  - sum_a ));//irq: servicing interrupts
         softirq_d = 100 * ((b[6] - a[6]) / ( sum_b  - sum_a));//softirq: servicing softirqs

         printf("The user CPU utilization at last %d second is : %Lf%%\n",time, user_d);
         printf("The nice CPU utilization at last %d second is : %Lf%%\n ",time, nice_d);
         printf("The system CPU utilization at last %d second is : %Lf%%\n",time, system_d);
         printf("The idle CPU utilization at last %d second is : %Lf%%\n ",time, idle_d);
         printf("The iowait CPU utilization at last %d second is : %Lf%%\n",time, iowait_d);
         printf("The irq CPU utilization at last %d second is : %Lf%%\n",time, irq_d);
         printf("The softirq CPU utilization at last %d second is : %Lf%%\n \n\n\n",time, softirq_d);
        }

	

    }

    return(0);
}
   
